import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'dart:math';

enum ChartType {
  BTC,
  ETH,
  OTHER
}

extension ChartTypeExtension on ChartType {

  double get threshold {
    switch (this) {
      case ChartType.BTC:
        return 60000;
      case ChartType.ETH:
        return 5000;
      case ChartType.OTHER:
        return 10000;
    }
  }


  List get dataList {
    int row = 13;
    int col = 2;

    var twoDList = List.generate(row, (i) => List.filled(col, 1.0, growable: false), growable: false);
    Random random = new Random();

    switch (this) {
      case ChartType.BTC:

        twoDList[0][0] = 0;
        twoDList[0][1] = 8905;
        twoDList[1][0] = 1;
        twoDList[1][1] = 9252;
        twoDList[2][0] = 2;
        twoDList[2][1] = 9708;
        twoDList[3][0] = 3;
        twoDList[3][1] = 11378;
        twoDList[4][0] = 4;
        twoDList[4][1] = 10729;
        twoDList[5][0] = 5;
        twoDList[5][1] = 13008;
        twoDList[6][0] = 6;
        twoDList[6][1] = 18746;
        twoDList[7][0] = 7;
        twoDList[7][1] = 24581;
        twoDList[8][0] = 8;
        twoDList[8][1] = 32500;
        twoDList[9][0] = 9;
        twoDList[9][1] = 48291;
        twoDList[10][0] = 10;
        twoDList[10][1] = 52173;
        twoDList[11][0] = 11;
        twoDList[11][1] = 48542;
        twoDList[12][0] = 12;
        twoDList[12][1] = 38085;
        return twoDList;
      case ChartType.ETH:
        twoDList[0][0] = 0;
        twoDList[0][1] = 204;
        twoDList[1][0] = 1;
        twoDList[1][1] = 232;
        twoDList[2][0] = 2;
        twoDList[2][1] = 305;
        twoDList[3][0] = 3;
        twoDList[3][1] = 383;
        twoDList[4][0] = 4;
        twoDList[4][1] = 354;
        twoDList[5][0] = 5;
        twoDList[5][1] = 405;
        twoDList[6][0] = 6;
        twoDList[6][1] = 567;
        twoDList[7][0] = 7;
        twoDList[7][1] = 621;
        twoDList[8][0] = 8;
        twoDList[8][1] = 1324;
        twoDList[9][0] = 9;
        twoDList[9][1] = 1524;
        twoDList[10][0] = 10;
        twoDList[10][1] = 1598;
        twoDList[11][0] = 11;
        twoDList[11][1] = 4076;
        twoDList[12][0] = 12;
        twoDList[12][1] = 2539;
        return twoDList;
      default:
        twoDList[0][0] = 0;
        twoDList[0][1] = random.nextInt(10000).toDouble();
        twoDList[1][0] = 1;
        twoDList[1][1] = random.nextInt(10000).toDouble();
        twoDList[2][0] = 2;
        twoDList[2][1] = random.nextInt(10000).toDouble();
        twoDList[3][0] = 3;
        twoDList[3][1] = random.nextInt(10000).toDouble();
        twoDList[4][0] = 4;
        twoDList[4][1] = random.nextInt(10000).toDouble();
        twoDList[5][0] = 5;
        twoDList[5][1] = random.nextInt(10000).toDouble();
        twoDList[6][0] = 6;
        twoDList[6][1] = random.nextInt(10000).toDouble();
        twoDList[7][0] = 7;
        twoDList[7][1] = random.nextInt(10000).toDouble();
        twoDList[8][0] = 8;
        twoDList[8][1] = random.nextInt(10000).toDouble();
        twoDList[9][0] = 9;
        twoDList[9][1] = random.nextInt(10000).toDouble();
        twoDList[10][0] = 10;
        twoDList[10][1] = random.nextInt(10000).toDouble();
        twoDList[11][0] = 11;
        twoDList[11][1] = random.nextInt(10000).toDouble();
        twoDList[12][0] = 12;
        twoDList[12][1] = random.nextInt(10000).toDouble();
        return twoDList;
    }
  }
}

class LineChartSample2 extends StatefulWidget {
  ChartType chartType;
  String cryptoName;
  LineChartSample2({required this.chartType, required this.cryptoName});
  @override
  _LineChartSample2State createState() => _LineChartSample2State();
}

class _LineChartSample2State extends State<LineChartSample2> {
  List<Color> gradientColors = [
    const Color(0xff23b6e6),
    const Color(0xff02d39a),
  ];

  bool showAvg = false;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        AspectRatio(
          aspectRatio: 1.70,
          child: Container(
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(18),
                ),
                color: Color(0xff232d37)),
            child: Padding(
              padding: const EdgeInsets.only(right: 18.0, left: 12.0, top: 24, bottom: 12),
              child: LineChart(
                mainData(),
              ),
            ),
          ),
        ),
        SizedBox(
          width: 60,
          height: 34,
          child: TextButton(
            onPressed: () {
              setState(() {
                showAvg = !showAvg;
              });
            },
            child: Text(
              widget.cryptoName,
              style: TextStyle(
                  fontSize: 12, color: showAvg ? Colors.white.withOpacity(0.5) : Colors.white),
            ),
          ),
        ),
      ],
    );
  }

  LineChartData mainData() {
    return LineChartData(
      gridData: FlGridData(
        show: true,
        drawVerticalLine: true,
        getDrawingHorizontalLine: (value) {
          return FlLine(
            color: const Color(0xff37434d),
            strokeWidth: 1,
          );
        },
        getDrawingVerticalLine: (value) {
          return FlLine(
            color: const Color(0xff37434d),
            strokeWidth: 1,
          );
        },
      ),
      titlesData: FlTitlesData(
        show: true,
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          getTextStyles: (value) =>
          const TextStyle(color: Color(0xff68737d), fontWeight: FontWeight.bold, fontSize: 16),
          getTitles: (value) {
            switch (value.toInt()) {
              case 0:
                return 'MAY';
              case 6:
                return 'NOV';
              case 12:
                return 'MAY';
            }
            return '';
          },
          margin: 8,
        ),
        leftTitles: SideTitles(
          showTitles: true,
          getTextStyles: (value) => const TextStyle(
            color: Color(0xff67727d),
            fontWeight: FontWeight.bold,
            fontSize: 15,
          ),
          getTitles: (value) {
            switch (widget.chartType) {
              case ChartType.BTC:
              switch (value.toInt()) {
                case 10000:
                  return '10k';
                case 30000:
                  return '30k';
                case 50000:
                  return '50k';
              }
              break;
              case ChartType.ETH:
                switch (value.toInt()) {
                  case 0:
                    return '0';
                  case 2000:
                    return '2k';
                  case 4000:
                    return '4k';
                }
                break;
              case ChartType.OTHER:
                switch (value.toInt()) {
                  case 2500:
                    return '2.5k';
                  case 5000:
                    return '5k';
                  case 7500:
                    return '7.5k';
                  case 10000:
                    return '10k';
                }
                break;
            }

            return '';
          },
          reservedSize: 28,
          margin: 12,
        ),
      ),
      borderData:
      FlBorderData(show: true, border: Border.all(color: const Color(0xff37434d), width: 1)),
      minX: 0,
      maxX: 12,
      minY: 0,
      maxY: widget.chartType.threshold,
      lineBarsData: [
        LineChartBarData(
          spots: [
            FlSpot(widget.chartType.dataList[0][0], widget.chartType.dataList[0][1]),
            FlSpot(widget.chartType.dataList[1][0], widget.chartType.dataList[1][1]),
            FlSpot(widget.chartType.dataList[2][0], widget.chartType.dataList[2][1]),
            FlSpot(widget.chartType.dataList[3][0], widget.chartType.dataList[3][1]),
            FlSpot(widget.chartType.dataList[4][0], widget.chartType.dataList[4][1]),
            FlSpot(widget.chartType.dataList[5][0], widget.chartType.dataList[5][1]),
            FlSpot(widget.chartType.dataList[6][0], widget.chartType.dataList[6][1]),
            FlSpot(widget.chartType.dataList[7][0], widget.chartType.dataList[7][1]),
            FlSpot(widget.chartType.dataList[8][0], widget.chartType.dataList[8][1]),
            FlSpot(widget.chartType.dataList[9][0], widget.chartType.dataList[9][1]),
            FlSpot(widget.chartType.dataList[10][0], widget.chartType.dataList[10][1]),
            FlSpot(widget.chartType.dataList[11][0], widget.chartType.dataList[11][1]),
            FlSpot(widget.chartType.dataList[12][0], widget.chartType.dataList[12][1]),
          ],
          isCurved: true,
          colors: gradientColors,
          barWidth: 5,
          isStrokeCapRound: true,
          dotData: FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(
            show: true,
            colors: gradientColors.map((color) => color.withOpacity(0.3)).toList(),
          ),
        ),
      ],
    );
  }
}